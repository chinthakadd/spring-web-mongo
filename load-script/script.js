import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  stages: [
    { duration: '30s', target: 20 },
    { duration: '1m30s', target: 1000 },
    { duration: '20s', target: 20 },
  ],
};

export default function() {
  let res = http.get('http://localhost:8080/blocking/customers');
  check(res, { 'status was 200': r => r.status == 200 });
  sleep(1);
}
