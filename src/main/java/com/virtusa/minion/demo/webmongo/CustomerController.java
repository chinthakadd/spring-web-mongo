package com.virtusa.minion.demo.webmongo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.concurrent.atomic.AtomicInteger;

import static org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE;

@RestController
public class CustomerController {

    private static Logger logger = LoggerFactory.getLogger(CustomerController.class);
    private AtomicInteger atomicInteger = new AtomicInteger();
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping(path = "/blocking/customers", produces = APPLICATION_STREAM_JSON_VALUE)
    public Flux<Customer> getBlockingCustomer() {
        logger.info("Thread: {}, Total Request Count: {}", Thread.currentThread().getName(),
                atomicInteger.incrementAndGet());
        return Flux.fromIterable(customerRepository.findAll());
    }
}
